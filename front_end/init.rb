require 'sinatra/base'

class FrontEnd < Sinatra::Base

  set :public_folder, Proc.new { File.join(root, "app") }
  enable :inline_templates

  get '*' do
    erb :index
  end

end

__END__

@@ layout
<!doctype html>
<html ng-app='app'>
  <body>
    <%= yield %>
  </body>
</html>


@@ index
<script src="vendor/angular.js"></script>
<script src="app.js"></script>


<div ng-app='app'>
  <div ng-controller='AppController'>
    <div ng-view></div>
  </div>
</div>

