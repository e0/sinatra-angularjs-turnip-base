var app = angular.module("app", []);

app.config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    $locationProvider.html5Mode(true);

    $routeProvider.
        when('/', {templateUrl: 'app.html'}).
        when('/another_page', {template: 'hello again from another page'}).
        when('/api', {template: 'you should not see this because server\'s api comes first, so you might as well delete this line'}).
        otherwise({template: 'client 404'});
}]);

app.controller('AppController', function ($scope) {
    $scope.model = {
        greeting: "hello world"
    };
});
