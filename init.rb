require_relative 'front_end/init'
require_relative 'back_end/init'

Router = Rack::URLMap.new(
  "/"    => FrontEnd.new,
  "/api" => BackEnd.new
)
