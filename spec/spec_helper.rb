ENV['RACK_ENV'] = 'test'

require_relative '../init'
Dir[File.dirname(__FILE__) + '/steps/*.rb'].each {|file| require file }

require 'turnip/capybara'
Capybara.server_port = 8081
Capybara.app = Router

# Capybara.default_driver = :selenium
require 'capybara/poltergeist'
Capybara.default_driver = :poltergeist

require 'rack/test'

RSpec.configure do |config|
  config.include FrontEndSteps

  def app
    Router
  end
  config.include Rack::Test::Methods

  config.include BackEndSteps
end
