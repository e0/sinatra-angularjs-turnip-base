module FrontEndSteps

  step "I am on the homepage" do
    visit '/'
  end

  step "I should see :greeting" do |greeting|
    page.has_content?(greeting).should == true
  end

end
