module BackEndSteps

  step "I make a request to :path" do |path|
    get path
  end

  step "I should get :response" do |response|
    last_response.body.should == response
  end

end
