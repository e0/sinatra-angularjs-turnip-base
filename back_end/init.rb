require 'sinatra/base'
require 'json'

class BackEnd < Sinatra::Base

  get '/' do
    'server'
  end

  get '/test' do
    [1,2,3].to_json
  end

  # make sure all api routes are before the get '*' one, which points to a back_end 404 instead of angularjs
  get '*' do
    not_found
  end

  not_found do
    'server 404'
  end

end
